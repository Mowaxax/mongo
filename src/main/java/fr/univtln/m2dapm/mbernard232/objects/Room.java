package fr.univtln.m2dapm.mbernard232.objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "rooms")
public class Room {

    /**
     * The Room ID
     */
    @Id
    private long id;

    /**
     * The Room name
     */
    private String name;

    /**
     * The Room location
     */
    private String location;

    public Room() {
        // Required by Spring
    }

    public Room(long id) {
        this(id, null, null);
    }

    public Room(long id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", name='" + name + "'" + ", location='" + location + "'" + '}';
    }
}
