package fr.univtln.m2dapm.mbernard232;

import fr.univtln.m2dapm.mbernard232.mongo.MongoDBUtils;
import fr.univtln.m2dapm.mbernard232.objects.Room;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App
{
    private static int mainMenu() throws IOException {
        int choice;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("");
            System.out.println("What do you want to do?");
            System.out.println("\t1) Show rooms");
            System.out.println("\t2) Show specific room");
            System.out.println("\t3) Add room");
            System.out.println("\t4) Remove room");
            System.out.println("\t5) Update room");
            System.out.println("\t0) Quit");
            System.out.print(" choice : ");
            choice = Integer.parseInt(in.readLine());
        }while(choice < 0 || choice > 5);

        return choice;
    }

    private static long selectRoomID(String action) throws IOException {
        long choice;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.print(" ID of room to " + action + " : ");
            choice = Long.parseLong(in.readLine());
        }while(choice < 0);

        return choice;
    }

    private static String selectRoomParam(String param) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.print(" " + param + " of the room : ");
        return in.readLine();
    }

    private static Room getRoomFromUser(String acton) throws IOException {
        long roomID = selectRoomID(acton);
        String roomName = selectRoomParam("name");
        String roomLocation = selectRoomParam("location");
        return new Room(roomID, roomName, roomLocation);
    }

    public static void main(String[] args) throws IOException {
        boolean process = true;

        // Process client instructions //
        do {
            switch(mainMenu())
            {
                case 0:
                    // End the process //
                    process = false;
                    break;
                case 1:
                    // Display rooms //
                    System.out.println(MongoDBUtils.getAllRooms());
                    break;
                case 2:
                    // Display room //
                    System.out.println(MongoDBUtils.getRoom(selectRoomID("display")));
                    System.out.println("");
                    break;
                case 3:
                    // Add room //
                    MongoDBUtils.createRoom(getRoomFromUser("create"));
                    break;
                case 4:
                    // Remove room //
                    MongoDBUtils.removeRoom(selectRoomID("remove"));
                    break;
                case 5:
                    // Update room //
                    long roomID = selectRoomID("update");
                    Room oldRoom = MongoDBUtils.getRoom(roomID);
                    if(oldRoom != null) {
                        Room newRoom = getRoomFromUser("update");
                        MongoDBUtils.updateRoom(oldRoom, newRoom);
                    }
                    else {
                        System.out.println("No room found for the given ID : " + roomID);
                    }
                    break;
                default:
                    System.err.println("Unhandled case selected by the user");
                    break;
            }
        }while(process);
    }
}
