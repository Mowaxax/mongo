package fr.univtln.m2dapm.mbernard232.mongo;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

@Configuration
public class MongoDBConfig extends AbstractMongoConfiguration {

    @Override
    @Bean
    public MongoClient mongoClient() {
        return new MongoClient("mongodbtest", 27017);
    }

    @Override
    protected String getDatabaseName() {
        return "storage";
    }

    @Override
    public @Bean
    MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(mongoClient(), getDatabaseName());
    }

    @Override
    public @Bean
    MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoDbFactory());
    }
}
