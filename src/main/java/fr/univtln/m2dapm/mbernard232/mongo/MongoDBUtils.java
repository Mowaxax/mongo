package fr.univtln.m2dapm.mbernard232.mongo;

import fr.univtln.m2dapm.mbernard232.objects.Room;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public class MongoDBUtils {

    private static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(MongoDBConfig.class);
    private static final MongoOperations MONGO_OPERATIONS = (MongoOperations) CONTEXT.getBean("mongoTemplate");

    private MongoDBUtils() {
        // Noting to do
    }

    public static void createRoom(Room room) {
        MONGO_OPERATIONS.save(room);
    }

    public static void updateRoom(Room oldRoom, Room newRoom) {
        if(oldRoom.getId() != newRoom.getId()) {
            removeRoom(oldRoom); // IDs are immutable, so we cannot update the ID and have to remove the old room //
            createRoom(newRoom);
        } else {
            // Query to search the old room //
            Query searchRoomsQuery = new Query(Criteria.where("_id").is(oldRoom.getId()));

            // Update to perform //
            Update updateName = Update.update("name", newRoom.getName());
            Update updateLocation = Update.update("location", newRoom.getLocation());

            // Perform the update //
            MONGO_OPERATIONS.update(Room.class).matching(searchRoomsQuery).apply(updateName).first();
            MONGO_OPERATIONS.update(Room.class).matching(searchRoomsQuery).apply(updateLocation).first();
        }
    }

    public static void removeRoom(Room room) {
        MONGO_OPERATIONS.remove(room);
    }

    public static void removeRoom(long roomID) {
        Query searchRoomQuery = new Query(Criteria.where("_id").is(roomID));
        MONGO_OPERATIONS.remove(searchRoomQuery, Room.class);
    }

    public static List<Room> getAllRooms() {
        return MONGO_OPERATIONS.findAll(Room.class);
    }

    public static Room getRoom(long roomID) {
        Query searchRoomQuery = new Query(Criteria.where("_id").is(roomID));
        return MONGO_OPERATIONS.findOne(searchRoomQuery, Room.class);
    }
}
