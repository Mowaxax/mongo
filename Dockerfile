FROM java:8
COPY target/mongodb-1.0-SNAPSHOT-jar-with-dependencies.jar /usr/src/client_rest/
WORKDIR /usr/src/client_rest/
ENTRYPOINT java -cp mongodb-1.0-SNAPSHOT-jar-with-dependencies.jar fr.univtln.m2dapm.mbernard232.App
